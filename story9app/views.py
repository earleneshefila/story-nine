from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import login, authenticate,logout

# Create your views here.

response = {}

def views_login(request):
    if request.method == "POST":
         username = request.POST['username']
         password = request.POST['password']
         user = authenticate(request, username=username, password=password)
         if user is not None:
            login(request, user)
            request.session['username'] = username
            response['username'] = request.session['username']
            return redirect('login')
         else:
            return HttpResponse("Invalid Password or Username")
    else:
        return render(request, 'login.html',response)

def views_logout(request):
    request.session.flush()
    logout(request)
    return redirect('login')
